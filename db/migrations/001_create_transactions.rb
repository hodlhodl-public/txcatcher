Sequel.migration do
  change do
    create_table(:transactions) do
      primary_key :id, type: :Bignum
      String :txid, index: true
      String :hex,  text: true
      Bignum :amount
      Integer :block_height
    end
  end
end
