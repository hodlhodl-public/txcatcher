require 'rubygems'
require 'crypto-unit'
require_relative '../../lib/txcatcher/utils/crypto_unit'

RSpec.describe TxCatcher::CryptoUnit do

  it "doesn't raise error when an amount of more than 21 million units is used on Litecoin" do
    expect( -> { TxCatcher::CryptoUnit.new(:ltc, 2100000000000001, from_unit: :primary) }).not_to raise_error
    expect( -> { TxCatcher::CryptoUnit.new(:btc, 2100000000000001, from_unit: :primary) }).to raise_error(CryptoUnit::TooLarge)
  end

end
