require 'rubygems'
require 'ffi-rzmq'

require_relative 'spec_helper'
require_relative '../lib/txcatcher/models/address'
require_relative '../lib/txcatcher/models/transaction'
require_relative '../lib/txcatcher/catcher'

RSpec.describe TxCatcher::Catcher do

  # None of the unit tests here should require a running bitcoind
  describe "(unit tests that require bitcoind to test)" do

    before(:all) do
      @tx_sock     = ZMQ::Context.create.socket(ZMQ::PUB)
      @block_sock  = ZMQ::Context.create.socket(ZMQ::PUB)

      @tx_sock.bind("ipc:///tmp/bitcoind_test.rawtx")
      @block_sock.bind("ipc:///tmp/bitcoind_test.hashblock")

      @hextx = File.read(File.dirname(__FILE__) + "/fixtures/transaction.txt").strip
      @rawtx = unhexlify(File.read(File.dirname(__FILE__) + "/fixtures/transaction.txt"))
      @catcher = TxCatcher::Catcher.new(name: "bitcoind_test")
      sleep 1
    end

    after(:all) do
      @catcher.close_all_connections
      @tx_sock.unbind("ipc:///tmp/bitcoind_test.rawtx")
      @block_sock.unbind('ipc:///tmp/bitcoind_test.hashblock')
    end

    after(:each) do
      @catcher.sockets["rawtx"][:last_message] = nil
      @catcher.sockets["hashblock"][:last_message] = nil
    end

    it "creates a new transaction in the DB" do
      @tx_sock.send_strings(['rawtx', @rawtx])
      i = 0
      until (tx = TxCatcher::Transaction.last) || i > 3
        sleep 1
        i+=1
      end
      expect(tx.hex).to eq(@hextx)
    end

    it "updates transactions block height upon receiving a new block " do
      transaction = TxCatcher::Transaction.create(hex: @hextx)
      expect(TxCatcher.rpc_node).to receive(:getblock).at_least(:once).and_return({ "height" => TxCatcher.current_block_height + 1, "tx" => [transaction.txid]})

      @block_sock.send_strings(["hashblock", 'hello'])

      i = 0
      begin
        sleep 1 and i += 1
      end until @catcher.sockets["hashblock"][:last_message] || i > 3

      expect(transaction.reload.block_height).to eq(TxCatcher.current_block_height)
    end

    it "ignores validation errors" do
      tx = eval File.read(File.dirname(__FILE__) + "/fixtures/transaction_decoded_no_outputs.txt")
      expect(TxCatcher.rpc_node).to receive(:decoderawtransaction).at_least(:once).and_return(tx)
      @tx_sock.send_strings(["rawtx", @rawtx])

      i = 0
      begin
        sleep 1 and i += 1
      end until @catcher.sockets["rawtx"][:last_message] || i > 3

      expect(File.exists?(ERRFILE)).to be_falsey
    end

    it "logs all other errors" do
      sleep 1
      expect(TxCatcher.rpc_node).to receive(:decoderawtransaction).at_least(:once).and_raise(StandardError)
      @tx_sock.send_strings(["rawtx", @rawtx])

      i = 0
      begin
        sleep 1 and i += 1
      end until @catcher.sockets["rawtx"][:last_message] || i > 3
      expect(File.read(ERRFILE)).not_to be_empty
    end

  end


  it "updates deposits association with an RBF tx if a transaction with lower fee gets confirmed" do

    class TxCatcher::Transaction
      def assign_tx_hash(h)
        @tx_hash = h
      end
    end

    class CatcherStub < TxCatcher::Catcher
      def listen_to_zeromq_channels(channel);end
    end

    hextx = File.read(File.dirname(__FILE__) + "/fixtures/transaction.txt").strip
    tx1 = TxCatcher::Transaction.create(hex: hextx)
    deposits = tx1.deposits
    rbf_tx_hash = tx1.tx_hash
    rbf_tx_hash["txid"] = "rbftxid1"
    rbf_tx_hash["locktime"] = "1"
    tx2 = TxCatcher::Transaction.new(hex: hextx)
    tx2.assign_tx_hash(rbf_tx_hash)
    tx2.save

    catcher = CatcherStub.new(name: "bitcoind_test")
    allow(TxCatcher.rpc_node).to receive(:getblock).and_return({ "tx" => [tx1.txid], "height" => 123 })
    catcher.send(:handle_hashblock, "new block")
    sleep 1

    deposits.each do |d|
      expect(d.reload.transaction_id).to eq(tx1.id)
    end

    expect(tx1.reload.deposits.map(&:id)).to eq(deposits.map(&:id))
    expect(tx2.reload.deposits.map(&:id)).to eq([])
  end

end
