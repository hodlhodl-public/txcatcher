require 'rubygems'
require 'yaml'
require 'json'
require 'sequel'
require_relative '../txcatcher/utils/hash_string_to_sym_keys'
require_relative '../txcatcher/logger'
require_relative '../txcatcher/config'
require_relative '../txcatcher/initializer'

Sequel.extension :migration

namespace :db do

  task :environment do
    include TxCatcher::Initializer
    ConfigFile.set!
    create_config_files
    read_config_file
    connect_to_db
  end

  desc "Migrates the database"
  task :migrate, [:step] => :environment do |t, args|
    target = args[:step] && (step = args[:step].to_i) > 0 ?
               current_migration_version + step : nil

    Sequel::Migrator.run(TxCatcher.db_connection, MIGRATIONS_ROOT, target: target)
    puts "Migrated DB to version #{current_migration_version}"
    dump_schema
  end

  desc "Rollbacks database migrations"
  task :rollback, [:step] => :environment do |t, args|
    target = args[:step] && (step = args[:step].to_i) > 0 ?
      current_migration_version - step : current_migration_version - 1

    Sequel::Migrator.run(TxCatcher.db_connection, MIGRATIONS_ROOT, target: target)
    puts "Rolled back DB to version #{current_migration_version}"
    dump_schema
  end

  def current_migration_version
    db = TxCatcher.db_connection
    Sequel::Migrator.migrator_class(MIGRATIONS_ROOT).new(db, MIGRATIONS_ROOT, {}).current
  end

  def dump_schema
    TxCatcher.db_connection.extension :schema_dumper
    open('db/schema.rb', 'w') do |f|
      f.puts TxCatcher.db_connection.dump_schema_migration(same_db: false)
    end
  end
end
