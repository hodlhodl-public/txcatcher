module TxCatcher

  class Address < Sequel::Model
    one_to_many :deposits

    def received_in_btc
      CryptoUnit.new(Config["currency"], self.received, from_unit: :primary).to_standart
    end

    def self.find_or_catch_and_create(a)
      # Even if there are not transactions to this address yet, we still create it,
      # because calling this method means someone is interested in this address and we need
      # to track it and maybe force catching it not just through mempool and ZeroMQ (see catcher.rb),
      # but also directly through querying RPC (slow, but at least we won't miss it).
      Address.find_or_create(address: a)

      if addr&.deposits.empty? && addr.created_at < (Time.now - 3600)
        # The address is in the DB, which means someone has been checking it,
        # but no deposits were associated with it and it's been more than 1 hour
        # since someone first got interested. Let's query the RPC directly, see if there were any transactions to this
        # address.

      end
    end

  end


end
