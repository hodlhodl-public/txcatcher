TxCatcher
========

*TxCatcher* is a lightweight alternative to Bitpay's Insight (bitcoin and litecoin compatible)
The purpose of this software is to receive all incoming unconfirmed transactions,
store them in the DB along with addresses (outputs) and provide a simple API to check whether
a certain address received a deposit.

However, it doesn't rely on having full blockchain with either bitcoind or litecoind.
You can have pruning enabled (in fact, it's recommended) and the txcatcher's DB
is regularly cleaned, so it doesn't grow.

The point is to detect new transactions to an address and store them for at least a while,
but then get rid of them. It's ideal for lightweight payment processing.

API requests:
-------------
request: `/addr/2N8Xdi8FiWAT3Vuo2mzcCETnrBjeAtBiqiA`
response (JSON):

    {
      "address":"2N8Xdi8FiWAT3Vuo2mzcCETnrBjeAtBiqiA","received":2000000
      "deposits":[
        {
          "txid":"4e6e1e491108b8302da8885b7e57b8809385d206346b3d287b6300408efe551c",
          "amount":0.01,
          "satoshis":1000000,
          "confirmations":0,
          "block_height":null
        }
    }

request: `/addr/2N8Xdi8FiWAT3Vuo2mzcCETnrBjeAtBiqiA/utxo`
response (JSON):

    [{
      "value":0.01,
      "n":0,
      "scriptPubKey":{"asm":"OP_HASH160 a7a458022e92dd04935c849ab2ea3b07373b7e4b OP_EQUAL","hex":"a914a7a458022e92dd04935c849ab2ea3b07373b7e4b87","reqSigs":1,"type":"scripthash","addresses":["2N8Xdi8FiWAT3Vuo2mzcCETnrBjeAtBiqiA"]},
      "confirmations":5,
      "txid":"4e6e1e491108b8302da8885b7e57b8809385d206346b3d287b6300408efe551c"
    }]

You can also broadcast transactions by sending requests to `/tx/send` passing transaction in `rawtx` param.


Is that all?
-----------
For now - yes. It allows you to do basic stuff like checking what an address has received.

TODO:
  * websockets for instant update notifications
  * better configuration through command line args


What it does under the hood
---------------------------
* it uses bitcoind or litecoind with ZeroMQ to receive new transactions from the mempool
* it receives newly mined blocks so that we can tell how many confirmations a transaction to a certain
  address has.
* regularly cleans the DB

Installation
------------

  Install dependencies:
  *  zeromq
  *  rvm, ruby & rubygems
  *  sqlite3 or postgresql
  *  bitcoind or litecoind or both

  Install txcatcher: `gem install txcatcher`

Configuring & Running
--------------------
You're going to need a simple config file. By default, txcatcher looks in `~/.txcatcher` dir,
so create the dir and the file there: `mkdir ~/.txcatcher && touch ~/.txcatcher/config.yml`
Example config file can be found in [templates/config.yml](templates/config.yml), you might want
to copy the contents and edit it.

Once your bitcoin/litecoind finished syncing, you can start txcatcher with a simple command: `txcatcher`.
You can start multiple instances of txcatcher, when, for example, you want instances of it
tracking both litecoin and bitcoin transactions:

    txcatcher -c ~/.txcatcher/litecoin_config.yml
    txcatcher -c ~/.txcatcher/bitcoin_config.yml
    
systemd & Upstart scripts
-------------
If running on Ubuntu, you may also want to create a systemd or an Upstart script, see templates:

  * [systemd template for txcatcher](templates/systemd_script.service)
  * [upstart template for txcatcher](templates/upstart_script)

Obviously, copy the file and place it into the appropriate dir:
  * `/etc/services/txcatcher_btc_mainnet.service` for systemd
  * `/etc/init.d/txcatcher_btc_mainnet` for upstart
  
 For this to work, you need a number of prerequisites:
 * create a `deploy` user, make sure rvm works with this user (rvm multi-user installation)
 * DO NOT daemonize the process in config.yml, set `daemonize: false` in `/home/deploy/.txcatcher/config.yml`
 * Generate rvm wrapper for txcatcher: https://rvm.io/deployment/init-d
 * Make sure that bitcoind (or litecoind) is running before starting txcatcher. This can be done
   on system level so bitcoind always starts before txcatcher (see upstart or systemd documentation).
 
You can then start and stop txcatcher with `sudo service txcatcher_btc start` and `sudo service txcatcher_btc stop`
for systemd and `sudo start txcatcher_btc` and `sudo stop txcatcher_btc` for upstart.

Running unit tests
------------------
Before running unit tests, make sure you have bitcoind running in mainnet mode,
with RPC enabled and blockchain synced. Copy the `spec/config/config.yml.sample` file
and change the RPC username/password to match your bitcoind settings.
